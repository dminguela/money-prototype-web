'use strict';

module.exports = {
  host: 'localhost',
  port: 3003,

  // app directories
  appDir: 'app',
  devDir: '',

  // unit test directories
  unitTestDir: 'app',

  expressServer: 'server/',
  projectServer: '../preview/app',
  buildDir: '../preview/app/',
  buildAppConfig: '../preview/app/config/',
  buildCss: '../preview/app/css/',
  buildCssVendor: '../preview/app/css/vendors/',
  buildFonts: '../preview/app/fonts/',
  buildImages: '../preview/app/images/',
  buildJs: '../preview/app/js/',
  buildJsVendor: '../preview/app/js/vendors/',
  buildJsVendorAux: '../preview/app/js/vendors/aux/',
  buildLanguages: '../preview/app/i18n/',
  buildLibs: '../preview/app/libs/'
};
