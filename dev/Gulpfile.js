'use strict';

var gulp = require('gulp');

require('require-dir')('./gulp');

gulp.task('dev', ['cleanBeforeBuild', 'build'], function () {
	gulp.start('browserSync');
	gulp.start('watch');
});

gulp.task('default', ['dev']);
gulp.task('dist', ['cleanBeforeBuild', 'build']);