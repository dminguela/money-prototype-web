(function() {
  'use strict';

  angular
    .module('globals')
    .directive('imageFocus', imageFocus);

  function imageFocus() {
    return {
      restrict: 'A',
      templateUrl: 'globals/image-focus/image-focus.tpl.html',

      link: function(scope, element, attrs) {
        scope.focusX = attrs.focusX;
        scope.focusY = attrs.focusY;
        scope.focusW = attrs.focusW;
        scope.focusH = attrs.focusH;
        scope.image = new Image();
        scope.image.onload = function() {
          $('.focuspoint').focusPoint();
        };
        scope.image.src = '/images/' + attrs.image + '.jpg';
        scope.imageTitle = attrs.image;
      }
    };
  }

})();
