(function() {
  'use strict';
  /* @ngdoc object
   * @name globals
   * @requires $stateProvider
   *
   * @description
   *
   */
  angular
    .module('globals', []);

})();
