(function() {
  'use strict';

  /* @ngdoc object
	 * @name globals.service:DataLayerService
	 *
	 * @description
	 *
	 */

  angular
    .module('globals')
    .factory('DataLayerService', DataLayerService);

  function DataLayerService($window) {
    var fm = this,
      dataLayer = dataLayer || [];
    fm.ControllerName = 'DataLayerService';

    function eventPush(category, action, label) {
      $window.dataLayer.push({
          'category': category,
          'action': action,
          'label': label,
          'event': 'eventga'
        });
    }

    function dimensionPush(key_, value_) {
      key_ = String(key_);
      value_ = String(value_);
      var key = key_;
      var obj = {};
      obj[key] = value_;
      $window.dataLayer.push(obj);
    }

    return {
      eventPush: eventPush,
      dimensionPush: dimensionPush
    };
  }

})();
