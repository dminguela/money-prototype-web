(function() {
  'use strict';

  angular
    .module('globals')
    .directive('customDimensions', customDimensions);

  function customDimensions(DatalayerService, $window, $rootScope, $cookies) {
    return {
      restrict: 'E',
      templateUrl: 'globals/gtm/custom-dimensions.tpl.html',

      link: function(scope, element, attrs) {
        var currentUser = '';
        scope.userId =  '';
        scope.notifications = '';

        scope.refreshData = function() {
          currentUser = $rootScope.globals.currentUser;
          scope.userId =  currentUser !== undefined ? currentUser.username : null;
          scope.notifications = $cookies.get('notifications') || false;
        };

        scope.dimensionSend = function(key, value) {
          DatalayerService.dimensionPush(key, value);
        };
      }
    };
  }

})();
