(function() {
  'use strict';

  /* @ngdoc object
   * @name interface.controller:LostPasswordService
   *
   * @description
   *
   */

  angular
    .module('globals')
    .factory('UserInfoService', UserInfoService);

  function UserInfoService($http, $q, $rootScope, InitialConfigService) {
    var fm = this;
    fm.ControllerName = 'UserInfoService';

    function setUserInfo(type) {
      var deferred = $q.defer(),
        config = InitialConfigService.getConfig(),
        promise = deferred.promise,
        req = {
          method: 'GET',
          url: config.API.url + config.API.urlsetUserInfo + '?cashInType=' + type,
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          }
        };

      $http(req).then(function(response) {
        switch (response.data.cresol.strCode) {
          case 'I1000':
            deferred.resolve();
            break;
          default:
            deferred.reject();
            break;
        }
      }, function(reason) {
        deferred.reject(reason.data.infoResponse) || false;
      });

      return deferred.promise;
    }

    function getUserSecurityQuestion(token) {
      var defered = $q.defer(),
        config = InitialConfigService.getConfig(),
        req = {
          method: 'GET',
          url: config.API.url + config.API.urlGetSecurityQuestion + token,
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          }
        };

      $http(req).then(function(response) {
        if (response.data.cresol.code == 'USERNOTFOUND') {
          defered.reject('USERNOTFOUND');
        } else {
          defered.resolve(response.data.cresol.text);
        }
      }, function(reason) {
        defered.reject(reason);
      });

      return defered.promise;
    }

    return {
      setUserInfo: setUserInfo,
      getUserSecurityQuestion: getUserSecurityQuestion
    };
  }
})();
