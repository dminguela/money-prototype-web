// jscs:disable
(function() {
  'use strict';

  /* @ngdoc object
   * @name globals.service:Branchio
   *
   * @description
   *
   */

  angular
    .module('globals')
    .service('Branchio', Branchio);

  function Branchio(localStorageService, $q) {
    var fm = this;

    function getDataBranch() {
      var deferred = $q.defer(),
        promise = deferred.promise,
        invalidLink = {
          amount: null,
          id: null,
          text: null,
          userId: null,
          url: null
        };

      branch.init('key_test_hpg3cyqS78ZmjiQfbEaaRjodwso5J6Ze', function(res, req) {
        fm.myReq = req;
        fm.myData = req.data;
        fm.deepLink = req.data_parsed;
        if (req.referring_link !== null) {
          fm.deepLink.customParameters.imageUrl = fm.deepLink.customParameters.imageUrl || null;
          localStorageService.remove('deepLink');
          localStorageService.remove('myReq');
          localStorageService.set('deepLink', fm.deepLink.customParameters);
          localStorageService.set('myReq', fm.myReq);
        }
        deferred.resolve(true);
      });
      return deferred.promise;
    }

     function sendSMS(number) {
      try {
        branch.first();
        branch.sendSMS(number, localStorageService.get('myReq'), function(err, data) {
          if (err != null) {
           
          } else {
           
          }
          console.log(err || JSON.stringify(data) || 'undefined');
        });
      } catch(e) {
        branch.init('key_test_hpg3cyqS78ZmjiQfbEaaRjodwso5J6Ze', function(res, req) {
          branch.sendSMS(number, localStorageService.get('myReq'), function(err, data) {
            if (err != null) {
             
            } else {
             
            }
            console.log(err || JSON.stringify(data) || 'undefined');
          });
        });
      }
    };

    return {
      getDataBranch: getDataBranch,
      sendSMS: sendSMS
    };
  }
})();
// jscs:enable
