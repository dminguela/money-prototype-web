(function() {
  'use strict';

  /* @ngdoc object
   * @name controller:LostPasswordService
   *
   * @description
   *
   */

  angular
    .module('globals')
    .factory('AuthenticationService', AuthenticationService);

  function AuthenticationService(Base64, $http, $cookieStore, $rootScope, $timeout) {
    var fm = this;

    function setCredentials(username, password) {
      var authdata = Base64.encode(username + ':' + password);

      $rootScope.globals = {
        currentUser: {
          username: username,
          authdata: authdata
        }
      };

      $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jscs:disable
      $cookieStore.put('globals', $rootScope.globals);
    }

    function getCredentials() {
      if ($rootScope.globals.currentUser !== null)
        return $rootScope.globals.currentUser;
      else 
        return null;
    }

    function clearCredentials() {
      $rootScope.globals = {};
      $cookieStore.remove('globals');
      $http.defaults.headers.common.Authorization = 'Basic ';
    }

    return {
      setCredentials: setCredentials,
      getCredentials: getCredentials,
      clearCredentials: clearCredentials
    };
  }
})();
