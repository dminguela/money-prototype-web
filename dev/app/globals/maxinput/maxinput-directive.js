(function() {
  'use strict';

  angular
    .module('globals')
    .directive('limitTo', limitTo);
  function limitTo() {
    return {
      restrict: 'A',
      link: function(scope, elem, attrs) {
        var limit = parseInt(attrs.limitTo);
        angular.element(elem).on('keypress', function(e) {
          if (this.value.length == limit) {
            e.preventDefault();
          }
        });
      }
    };
  }

})();
