(function() {
  'use strict';

  angular
    .module('globals')
    .directive('mainLanguages', mainLanguages);

  function mainLanguages() {
    return {
      restrict: 'E',
      templateUrl: 'globals/main/header/languages.tpl.html',
      replace: true,

      link: function(scope, element, attrs) {

        scope.languages = [
          {text: 'es', value: 'es'},
          {text: 'ca', value: 'ca'}
        ];

      }
    };
  }

})();
