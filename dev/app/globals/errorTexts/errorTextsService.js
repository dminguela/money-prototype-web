(function() {
  'use strict';

  /* @ngdoc object
   * @name controller:LostPasswordService
   *
   * @description
   *
   */

  angular
    .module('globals')
    .factory('ErrorTextsService', ErrorTextsService);

  function ErrorTextsService($http) {
    var fm = this,
        errorTexts;

    function getCresolErrorTexts() {
      $http.get('i18n/errorCresolTexts.json').then(function(response) {
        errorTexts = response.data.errors;
      }, function(reason) {
        console.log(reason);
      });
    }

    function searchCresolText(code) {
      var errorCode = 'error_cresol_' + code;
      var errorText;
      for (errorText in errorTexts) {
        if (errorText == errorCode) {
          return errorTexts[errorText];
        }
      }
    }

    return {
      getCresolErrorTexts: getCresolErrorTexts,
      searchCresolText: searchCresolText
    };
  }
})();
