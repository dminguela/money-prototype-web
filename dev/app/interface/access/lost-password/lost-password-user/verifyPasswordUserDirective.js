(function() {
  'use strict';

  angular
    .module('lostPassword')
    .directive('passwordVerify', passwordVerify);

  function passwordVerify() {
    return {
      require: 'ngModel',
      scope: {
        passwordVerify: '='
      },
      link: function(scope, element, attrs, Controller) {
        scope.$watch(function() {
          var combined;

          if (scope.passwordVerify || Controller.$viewValue) {
            combined = scope.passwordVerify + '_' + Controller.$viewValue;
          }

          return combined;
        }, function(value) {
          if (value) {
            Controller.$parsers.unshift(function(viewValue) {
              var origin = scope.passwordVerify;
              if (origin !== viewValue) {
                Controller.$setValidity('passwordVerify', false);
                return undefined;
              } else {
                Controller.$setValidity('passwordVerify', true);
                return viewValue;
              }
            });
          }
        });
      }
    };
  }
})();
