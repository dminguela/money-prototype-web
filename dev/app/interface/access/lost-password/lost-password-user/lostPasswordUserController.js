(function() {
  'use strict';

  /* @ngdoc object
   * @name controller:LostPasswordUserController
   *
   * @description
   *
   */

  angular
    .module('lostPassword')
    .controller('LostPasswordUserController', LostPasswordUserController);

  function LostPasswordUserController(getUserInfo, $location, $rootScope, LostPasswordService, $q,
    $state, $window) {
    var vm = this;
    vm.ControllerName = 'LostPasswordUserController';
    vm.error = false;
    vm.preguntas = [{
      text: 'Modelo primer coche',
      value: '01'
    }, {
      text: 'Mote infancia',
      value: '02'
    }, {
      text: 'Nombre mejor amigo',
      value: '03'
    }, {
      text: 'Nombre mascota',
      value: '04'
    }, {
      text: 'Película favorita',
      value: '05'
    }, {
      text: 'Apellido abuela materna',
      value: '06'
    }];

    switch (getUserInfo) {
      case 'USERNOTFOUND':
        vm.userNotFound = true;
        break;
      case null:
        vm.userOld = true;
        $rootScope.userOld = true;
        break;
      default:
        vm.userOld = false;
        $rootScope.userOld = false;
        vm.userSecurityQuestion = getUserInfo;
        break;
    }

    vm.recoverPassword = function() {
      if (vm.recoverPasswordForm.$valid) {
        if ($rootScope.userOld) {
          vm.dataToSend = {
            'eMail': $rootScope.emailToken.toString(),
            'key1': vm.password.toString(),
            'key2': vm.password.toString(),
            'securityQuestion': vm.securityQuestion.toString(),
            'securityAnswer': vm.securityAnswer.toString(),
            'birthDay': angular.element(document).find('select[name="day"]').val(),
            'birthMonth': angular.element(document).find('select[name="month"]').val(),
            'birthYear': angular.element(document).find('select[name="year"]').val(),
            'accLast6': vm.card || 999999
          };
        } else {
          vm.dataToSend = {
            'eMail': $rootScope.emailToken,
            'key1': vm.password.toString(),
            'key2': vm.password.toString(),
            'securityAnswer': vm.securityAnswer.toString()
          };
        }

        LostPasswordService.lostPassword(vm.dataToSend).then(function(result) {
          $state.go('lostPassword.confirmLostPassword');
        }).catch(function(reason) {
          vm.error = true;
          vm.errorText = reason;
        });
      }
    };

    vm.initializeBirthDay = function() {
      $window.DatetimeSelect.prototype.InitDatetime();
    };

  }
})();
