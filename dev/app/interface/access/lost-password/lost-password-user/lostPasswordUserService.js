(function() {
  'use strict';

  /* @ngdoc object
   * @name controller:LostPasswordService
   *
   * @description
   *
   */

  angular
    .module('lostPassword')
    .factory('LostPasswordService', LostPasswordService);

  function LostPasswordService($http, $q, $rootScope, InitialConfigService) {
    var fm = this,
      recoverPassword = {};
    fm.ControllerName = 'LostPasswordService';

    function lostPassword(dataToSend) {
      var defered = $q.defer(),
        promise = defered.promise,
        result = false,
        config = InitialConfigService.getConfig(),
        url = $rootScope.userOld ? config.API.urlRecoverPasswordOld : config.API.urlRecoverPassword,
        req = {
          method: 'POST',
          url: config.API.url + url,
          transformRequest: function(obj) {
            var str = [];
            for (var p in obj) {
              str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
            }
            return str.join('&');
          },
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          },
          data: dataToSend
        };

      $http(req).then(function(res) {
        if (response.data.cresol.strCode == 'I1000') {
          defered.resolve(true);
        } else {
          errorCresol = ErrorTextsService.searchCresolText(response.data.cresol.strCode);
          defered.reject(errorCresol);
        }
      }, function(res) {
        var errorCresol = ErrorTextsService.searchCresolText('DEFAULT');
        defered.reject(errorCresol);
      });

      return promise;
    }

    function set(data) {
      recoverPassword = data;
    }

    function get() {
      return recoverPassword;
    }

    return {
      lostPassword: lostPassword,
      set: set,
      get: get
    };
  }

})();
