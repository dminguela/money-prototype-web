(function() {
  'use strict';

  /* @ngdoc object
   * @name controller:ConfirmLostPasswordController
   *
   * @description
   *
   */

  angular
    .module('lostPassword')
    .controller('ConfirmLostPasswordController', ConfirmLostPasswordController);

  function ConfirmLostPasswordController($location, $scope) {
    var vm = this;
    vm.ControllerName = 'ConfirmLostPasswordController';
  }

})();
