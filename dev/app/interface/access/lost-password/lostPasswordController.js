(function() {
  'use strict';

  /* @ngdoc object
  * @name controller:LostPasswordController
  *
  * @description
  *
  */

  angular
    .module('lostPassword')
    .controller('LostPasswordController', LostPasswordController);

  function LostPasswordController() {
    var vm = this;
    vm.controller = 'LostPasswordController';
  }

})();
