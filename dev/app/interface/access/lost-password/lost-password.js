(function() {
  'use strict';

  /* @ngdoc object
   * @name interface
   * @requires $stateProvider
   *
   * @description
   *
   */

  angular
    .module('lostPassword', [
      'ui.router'
    ])
    .config(config);

  //jscs: disable
  function config($stateProvider) {
    $stateProvider
      .state('lostPassword', {
        url: '/lost-password',
        abstract: true,
        templateUrl: 'interface/access/lost-password/lost-password.tpl.html',
        controller: 'LostPasswordController as lostPassword',
      })
      .state('lostPassword.lostPasswordUser', {
        url: '/lost-password-user',
        templateUrl: 'interface/access/lost-password/' +
                    'lost-password-user/lost-password-user.tpl.html',
        controller: 'LostPasswordUserController as lostPasswordUser',
        resolve: {
          getUserInfo: function(InitialConfigService, UserInfoService, $location, $rootScope,
            $q) {
            var defered = $q.defer(),
              token = $location.search()['eMail']; // jscs:disable
            $rootScope.emailToken = token;
            InitialConfigService.getInitialConfig().then(function() {
              UserInfoService.getUserSecurityQuestion(token).then(function(response) {
                defered.resolve(response);
              }).catch(function(reason) {
                defered.resolve(reason);
              });
            });

            return defered.promise;
          }
        }
      })
      .state('lostPassword.confirmLostPassword', {
        url: '/confirm-lost-password',
        templateUrl: 'interface/access/lost-password/' +
                    'confirm-lost-password/confirm-lost-password.tpl.html',
        controller: 'ConfirmLostPasswordController as confirmLostPassword'
      });
  }
  //jscs: enable

})();
