(function() {
  'use strict';

  /* @ngdoc object
   * @name controller:AddCardController
   *
   * @description
   *
   */

  angular
    .module('register')
    .controller('AddCardController', AddCardController);

  function AddCardController($location, $rootScope, AddCardService, $q, $state, $window) {
    var vm = this,
      numberCard,
      expirateMonth;
    vm.ControllerName = 'AddCardController';

    vm.addCardUser = function() {
      if (vm.addCardForm.$valid) {
        numberCard = vm.numberOfCard.toString();
        expirateMonth = vm.expirateMonth < 10 ? '0' +
                                          vm.expirateMonth.toString() : vm.expirateMonth.toString();
        vm.dataToSend = {
          'accPan': numberCard,
          'alias': 'Tarjeta ****' + numberCard.substring(numberCard.length - 4),
          'cvv2': vm.cvv.toString(),
          'expiryDate': vm.expirateYear.toString() + expirateMonth,
          'defaultAcc': 'false'
        };

        AddCardService.addCard(vm.dataToSend).then(function(result) {
          $state.go('register.confirm');
        }).catch(function(reason) {
          vm.error = false;
          vm.modal = false;
          if (reason.needAcceptModal) {
            vm.modal = true;
            vm.modalText = reason.errorText;
          } else {
            vm.error = true;
            vm.errorText = reason.errorText;
          }
        });
      }
    };

    vm.modalOk = function() {
      $state.go('register.confirm');
    };

  }
})();
