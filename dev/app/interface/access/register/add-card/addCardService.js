(function() {
  'use strict';

  /* @ngdoc object
   * @name controller:AddCardService
   *
   * @description
   *
   */

  angular
    .module('register')
    .factory('AddCardService', AddCardService);

  function AddCardService($http, $q, $rootScope, InitialConfigService, AuthenticationService,
    ErrorTextsService) {
    var fm = this,
      errorCresol;
    fm.ControllerName = 'AddCardService';

    function addCard(dataToSend) {
      var defered = $q.defer(),
        config = InitialConfigService.getConfig(),
        req = {
          method: 'POST',
          url: config.API.url + config.API.urlAddCard,
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          },
          data: dataToSend
        };

      $http(req).then(function(response) {
        if (response.data.cresol.strCode == 'I1000') {
          defered.resolve(true);
        } else if (response.data.cresol.strCode == 'W8190') {
          errorCresol = {
            'errorText': ErrorTextsService.searchCresolText(response.data.cresol.strCode),
            'needAcceptModal': true
          };
          defered.reject(errorCresol);
        } else {
          errorCresol = {
            'errorText': ErrorTextsService.searchCresolText(response.data.cresol.strCode),
            'needAcceptModal': false
          };
          defered.reject(errorCresol);
        }
      }, function(reason) {
        errorCresol = ErrorTextsService.searchCresolText('DEFAULT');
        errorCresol = {
            'errorText': ErrorTextsService.searchCresolText(response.data.cresol.strCode),
            'needAcceptModal': false
          };
        defered.reject(errorCresol);
      });

      return defered.promise;
    }

    return {
      addCard: addCard
    };
  }

})();
