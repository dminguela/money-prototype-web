(function() {
  'use strict';

  /* @ngdoc object
  * @name controller:RegisterController
  *
  * @description
  *
  */

  angular
    .module('register')
    .controller('RegisterController', RegisterController);

  function RegisterController($state, $stateParams, getFromState) {
    var vm = this;
    vm.controller = 'RegisterController';
    vm.fromStatus = getFromState;
  }

})();
