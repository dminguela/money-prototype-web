(function() {
  'use strict';

  /* @ngdoc object
   * @name controller:RegisterService
   *
   * @description
   *
   */

  angular
    .module('register')
    .factory('OtpService', OtpService);

  function OtpService($http, $q, $rootScope, InitialConfigService, ErrorTextsService) {
    var fm = this;
    fm.ControllerName = 'OtpService';

    function sendOtp(idPhone) {
      var defered = $q.defer(),
        promise = defered.promise,
        config = InitialConfigService.getConfig(),
        req = {
          method: 'GET',
          url: config.API.url + config.API.urlSendOtp + '?idPhone=' + idPhone,
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          }
        };

      $http(req).then(function(response) {
        if (response.status == 200) {
          defered.resolve(true);
        } else {
          defered.reject(false);
        }
      }, function(reason) {
        var errorCresol = ErrorTextsService.searchCresolText('DEFAULT');
        defered.reject(errorCresol);
      });

      return promise;
    }

    function validateOtp(otp) {
      var defered = $q.defer(),
        promise = defered.promise,
        config = InitialConfigService.getConfig(),
        req = {
          method: 'GET',
          url: config.API.url + config.API.urlValidateOtp + '?otp=' + otp,
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          }
        };

      $http(req).then(function(response) {
        if (response.data.cresol.strCode == 'I1000') {
          defered.resolve(true);
        } else {
          var errorCresol = ErrorTextsService.searchCresolText(response.data.cresol.strCode);
          defered.reject(errorCresol);
        }
      }, function(reason) {
        var errorCresol = ErrorTextsService.searchCresolText('DEFAULT');
        defered.reject(errorCresol);
      });

      return promise;
    }

    return {
      sendOtp: sendOtp,
      validateOtp: validateOtp
    };
  }
})();
