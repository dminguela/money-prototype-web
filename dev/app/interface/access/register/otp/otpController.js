(function() {
  'use strict';

  /* @ngdoc object
   * @name register.controller:RegisterController
   *
   * @description
   *
   */

  angular
    .module('register')
    .controller('OtpController', OtpController);

  function OtpController($location, $state, $window, OtpService, RegisterService, $stateParams) {
    var vm = this;
    vm.ControllerName = 'OtpController';
    vm.idPhone = RegisterService.get().userInfo.phoneInfoes[0].id;
    vm.userData = RegisterService.get();

    vm.sendOtp = function() {
      OtpService.sendOtp(vm.idPhone).then(function(response) {
        vm.otpSended = true;
        vm.errorOtpSended = false;
      }).catch(function(reason) {
        vm.errorOtpSended = true;
        vm.errorOtpText = reason;
        vm.otpSended = false;
      });
    };

    vm.validateOtp = function(otp) {
      OtpService.validateOtp(otp).then(function(response) {
        $state.go('register.addCard');
      }).catch(function(reason) {
        vm.error = true;
        vm.errorText = reason;
      });
    };
  }

})();
