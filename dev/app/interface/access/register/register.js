(function() {
  'use strict';

  /* @ngdoc object
   * @name interface.controller:RegisterController
   *
   * @description
   *
   */

  angular
    .module('register', [
      'ui.router'
    ])
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('register', {
        url: '/:from/register',
        templateUrl: 'interface/access/register/register.tpl.html',
        controller: 'RegisterController as register',
        abstract: true,
        resolve: {
          getFromState: function($stateParams) {
            return $stateParams.from == 'payLink' ? false : true;
          }
        }
      })
      .state('register.registerUser', {
        url: '/register-user',
        templateUrl: 'interface/access/register/register-user/register-user.tpl.html',
        controller: 'RegisterUserController as registerUser',
      })
      .state('register.otp', {
        url: '/otp',
        templateUrl: 'interface/access/register/otp/otp.tpl.html',
        controller: 'OtpController as otp'
      })
      .state('register.addCard', {
        url: '/add-card',
        templateUrl: 'interface/access/register/add-card/add-card.tpl.html',
        controller: 'AddCardController as addCard'
      })
      .state('register.confirm', {
        url: '/confirm/:card',
        templateUrl: 'interface/access/register/confirm-user/confirm.tpl.html',
        controller: 'ConfirmRegisterUserController as confirmRegisterUser'
      })
      .state('register.confirmFinish', {
        url: '/confirm/:card/finish',
        templateUrl: 'interface/access/register/confirm-user/finish/finish.tpl.html',
        controller: 'FinishRegisterUserController as finishRegisterUser'
      });
  }

})();
