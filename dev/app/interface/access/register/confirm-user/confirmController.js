(function() {
  'use strict';

  /* @ngdoc object
   * @name register.controller:RegisterController
   *
   * @description
   *
   */

  angular
    .module('register')
    .controller('ConfirmRegisterUserController', ConfirmRegisterUserController);

  function ConfirmRegisterUserController($location, $state, $window, $stateParams,
    UserInfoService) {
    var vm = this;
    vm.ControllerName = 'ConfirmRegisterUserController';
    vm.check = true;
    vm.card = $stateParams.card == 'card' ? true : false;

    vm.recharge = function() {
      var type = vm.check ? 'T' : 'N';
      UserInfoService.setUserInfo(type);
    };
  }

})();
