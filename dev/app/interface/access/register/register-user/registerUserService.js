(function() {
  'use strict';

  /* @ngdoc object
   * @name controller:RegisterService
   *
   * @description
   *
   */

  angular
    .module('register')
    .factory('RegisterService', RegisterService);

  function RegisterService($http, $q, $rootScope, InitialConfigService, AuthenticationService,
    ErrorTextsService, localStorageService) {
    var fm = this,
    userRegister = {};
    fm.ControllerName = 'RegisterService';

    function registerUser(dataToSend) {
      var defered = $q.defer(),
        promise = defered.promise,
        config = InitialConfigService.getConfig(),
        req = {
          method: 'POST',
          url: config.API.url + config.API.urlRegisterOld,
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          },
          data: dataToSend
        };

      $http(req).then(function(response) {
        if (response.data.cresol.strCode == 'I1000') {
          userRegister = response.data;
          set(userRegister);
          AuthenticationService.setCredentials(dataToSend.userName, dataToSend.userPass);
          defered.resolve(true);
        } else {
          var errorCresol = ErrorTextsService.searchCresolText(response.data.cresol.strCode);
          defered.reject(errorCresol);
        }
      }, function(reason) {
        var errorCresol = ErrorTextsService.searchCresolText('DEFAULT');
        defered.reject(errorCresol);
      });

      return promise;
    }

    function set(data) {
      localStorageService.set('userRegister', data);
    }

    function get() {
      return localStorageService.get('userRegister');
    }

    return {
      registerUser: registerUser,
      set: set,
      get: get
    };
  }
})();
