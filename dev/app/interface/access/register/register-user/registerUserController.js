(function() {
  'use strict';

  /* @ngdoc object
   * @name controller:RegisterController
   *
   * @description
   *
   */

  angular
    .module('register')
    .controller('RegisterUserController', RegisterUserController);

  function RegisterUserController($location, RegisterService, $q, $state, $scope, $stateParams,
    getFromState) {
    var vm = this;

    vm.ControllerName = 'RegisterUserController';
    vm.error = false;
    vm.preguntas = [{
      text: 'Modelo primer coche',
      value: '01'
    }, {
      text: 'Mote infancia',
      value: '02'
    }, {
      text: 'Nombre mejor amigo',
      value: '03'
    }, {
      text: 'Nombre mascota',
      value: '04'
    }, {
      text: 'Película favorita',
      value: '05'
    }, {
      text: 'Apellido abuela materna',
      value: '06'
    }];

    vm.sendUser = function() {

      if (vm.registerUserForm.$valid) {
        vm.dataToSend = {
          'userName': vm.user.email.toString(),
          'eMail': vm.user.email.toString(),
          'areaCode': '+34',
          'phoneNumber': vm.user.phone.toString(),
          'userPass': vm.user.password.toString(),
          'name': vm.user.name.toString(),
          'canal': 'WEB',
          'securityQuestion': vm.user.securityQuestion.toString(),
          'securityAnswer': vm.user.securityAnswer.toString(),
          'codIdioma': 'es'
        };

        vm.loading = true;
        RegisterService.registerUser(vm.dataToSend).then(function(result) {
          vm.loading = false;
          $state.go('register.otp');
        }).catch(function(reason) {
          vm.loading = false;
          vm.error = true;
          vm.errorText = reason;
        });
      }
    };
  }
})();
