(function() {
  'use strict';

  /* @ngdoc object
   * @name login
   * @requires $stateProvider
   *
   * @description
   *
   */
  angular
    .module('login', [
      'ui.router'
    ])
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        abstract: true,
        templateUrl: 'interface/access/login/login.tpl.html',
        controller: 'LoginController as login'
      })
      .state('login.loginUser', {
        url: '/login-user',
        templateUrl: 'interface/access/login/login-user/login-user.tpl.html',
        controller: 'LoginUserController as loginUser'
      });
  }

})();
