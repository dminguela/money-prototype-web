(function() {
  'use strict';

  /* @ngdoc object
   * @name controller:LoginUserController
   *
   * @description
   *
   */
  angular
    .module('login')
    .controller('LoginUserController', LoginUserController);

  function LoginUserController($location, $q, $state, $scope, LoginUserService) {
    var vm = this;
    vm.ControllerName = 'LoginUserController';

    vm.sendUser = function() {

      if (vm.loginForm.$valid) {

        vm.userData = {
          'userName': vm.email.toString(),
          'userPass': vm.password.toString()
        };

        LoginUserService.loginUser(vm.userData).then(function(resultLogin) {
          // no hay nada después
        }).catch(function(error) {
          vm.errorServer = false;
          vm.errorNoConnection = false;
          if (error !== undefined) {
            vm.errorServer = true;
            vm.errorServerText = error;
          } else {
            vm.errorNoConnection = true;
          }
        });

      }
    };

  }

})();
