(function() {
  'use strict';

  /* @ngdoc object
   * @name controller:RegisterService
   *
   * @description
   *
   */

  angular
    .module('login')
    .factory('LoginUserService', LoginUserService);

  function LoginUserService($http, $q, AuthenticationService, $rootScope, InitialConfigService) {
    var fm = this,
      userLogin = {};
    fm.ServiceName = 'LoginUserService';

    // declaro la función enviar
    function loginUser(userData) {
      var defered = $q.defer(),
      promise = defered.promise,
      result = false,
      config = InitialConfigService.getConfig(),
      req = {
        method: 'GET',
        url: config.API.url + config.API.urlLogin,
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
      };

      AuthenticationService.setCredentials(userData.userName, userData.userPass);

      $http(req).then(function(response) {
        userLogin = response.data;
        defered.resolve(result);
      }, function(reason, status, headers, config) {
        AuthenticationService.clearCredentials();
        defered.reject(reason.data.infoResponse.text);
      });

      return promise;
    }

    function get() {
      return userLogin;
    }

    return {
      loginUser: loginUser,
      get: get
    };
  }

})();
