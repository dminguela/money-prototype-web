(function() {
  'use strict';

  /* @ngdoc object
   * @name splash-screen.controller:ConfirmPayController
   *
   * @description
   *
   */
  angular
    .module('payLink')
    .controller('ConfirmPayController', ConfirmPayController);

  function ConfirmPayController(localStorageService, DataLayerService) {
    var vm = this;
    vm.controllerName = 'ConfirmPayController';
    vm.payLinkParameters = localStorageService.get('deepLink');

    vm.sendEventPush = function(_category, _action, _label) {
      DataLayerService.eventPush(_category, _action, _label);
    };

    vm.share = function() {
      FB.ui(
      {
        method: 'feed',
        name: 'Yaap Money',
        link: 'https://www.yaap.com/money/',
        picture: 'https://www.yaap.com/money/wp-content/uploads/sites/3/2016/01/logos-yaap-money-verde.jpg', // jscs:ignore maximumLineLength
        caption: 'Envía dinero de móvil a móvil',
        description: 'He pagado un link de pago a través de Yaap Money',
        message: ''
      });
    };
  }

})();
