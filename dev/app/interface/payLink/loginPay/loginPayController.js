(function() {
  'use strict';

  /* @ngdoc object
   * @name interface.controller:LoginUserController
   *
   * @description
   *
   */
  angular
    .module('payLink')
    .controller('LoginPayController', LoginPayController);

  function LoginPayController($q, $state, LoginPayService, localStorageService, UserInfoService,
    LostPasswordSendEmailService, Branchio, DataLayerService) {
    var vm = this;
    vm.ControllerName = 'LoginPayController';
    vm.payLinkParameters = localStorageService.get('deepLink');

    vm.sendPay = function() {
      if (vm.loginForm.$valid) {

        vm.paymentLink = {
          'paymentLinkId': localStorageService.get('deepLink').id,
          'amount': localStorageService.get('amount') || null
        };

        vm.userData = {
          'userName': vm.email.toString(),
          'userPass': vm.password.toString()
        };
        vm.loading = true;
        LoginPayService.payLink(vm.userData, vm.paymentLink).then(function(resultPay) {
          vm.loading = false;
          if (resultPay.code == 'EXCEEDED_TRANSACTION_AMOUNT') {
            vm.needRecharge = true;
          } else {
            vm.sendEventPush('PayDeeplink', 'confirmPayment_OK', 'PayDeeplink-ConfirmPayment');
            $state.go('payLink.confirmPay');
          }
        }).catch(function(error) {
          vm.loading = false;
          vm.errorServer = false;
          vm.errorNoConnection = false;
          vm.errorBadCredentials = false;
          if (error !== undefined && error.text) {
            vm.errorServer = true;
            vm.errorServerText = error.text;
          } else if (error == 'badCredentials') {
            vm.errorBadCredentials = true;
          } else if (error !== undefined) {
            vm.errorNoConnection = true;
          } else {
            vm.errorNoConnection = true;
          }
          vm.sendEventPush('PayDeeplink', 'confirmPayment_KO', 'PayDeeplink-ConfirmPayment');
        });
      }
    };
    vm.sendSMS = function(number) {
      if (vm.sendSMSform.$valid) {
        var phone = '+34 ' + number;
        Branchio.sendSMS(phone);
      }
    };
    vm.recharge = function() {
      vm.loading = true;
      vm.errorBadCredentials = false;
      vm.errorServer = false;
      vm.needRecharge = false;
      UserInfoService.setUserInfo('T').then(function(response) {
        vm.sendPay();
      }).catch(function(response) {
        vm.loading = false;
        vm.errorNoConnection = true;
      });
    };

    vm.sendEmail = function() {
      if (vm.lostPasswordSendEmail.$valid) {
        vm.showSendPassword = false;
        vm.errorShowSendPassword = false;
        vm.errorShowSendPasswordNoConnection = false;
        LostPasswordSendEmailService.sendEmail(vm.resetEmail.toString()).then(function() {
          vm.showSendPassword = true;
        }).catch(function(reason) {
          vm.errorShowSendPassword = true;
          vm.errorShowSendPasswordText = reason;
        });
      }
    };

    vm.sendEventPush = function(_category, _action, _label) {
      DataLayerService.eventPush(_category, _action, _label);
    };
  }

})();
