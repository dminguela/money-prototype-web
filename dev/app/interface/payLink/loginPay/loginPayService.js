(function() {
  'use strict';

  /* @ngdoc object
   * @name interface.controller:RegisterService
   *
   * @description
   *
   */

  angular
    .module('payLink')
    .factory('LoginPayService', LoginPayService);
  function LoginPayService($http, $q, AuthenticationService, $rootScope, InitialConfigService) {
    var fm = this;
    var userLogin = {};
    fm.ServiceName = 'LoginUserService';
    // declaro la función enviar
    function payLink(userData, linkData) {
      var defered = $q.defer(),
        promise = defered.promise,
        config = InitialConfigService.getConfig(),
        req = {
          method: 'POST',
          url: config.API.url + config.API.urlPaymentLink,
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          },
          data: linkData
        };

      AuthenticationService.setCredentials(userData.userName, userData.userPass);
      $http(req).then(function(response) {
        if (response.data.infoResponse.code != 'OK' &&
          response.data.infoResponse.code != 'EXCEEDED_TRANSACTION_AMOUNT') {
          defered.reject(response.data.infoResponse);
        } else {
          defered.resolve(response.data.infoResponse);
        }
      }, function(reason, status, headers, config) {
        AuthenticationService.clearCredentials();
        if (reason.status == 401) {
          defered.reject('badCredentials');
        }
        defered.reject(reason.data.infoResponse.text);
      });
      return promise;
    }
    return {
      payLink: payLink
    };
  }

})();
