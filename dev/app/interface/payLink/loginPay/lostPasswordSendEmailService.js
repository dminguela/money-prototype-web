(function() {
  'use strict';

  /* @ngdoc object
   * @name interface.controller:LostPasswordSendEmailService
   *
   * @description
   *
   */

  angular
    .module('payLink')
    .factory('LostPasswordSendEmailService', LostPasswordSendEmailService);

  function LostPasswordSendEmailService($http, $q, $rootScope, InitialConfigService) {
    var fm = this;
    fm.ServiceName = 'LostPasswordSendEmailService';
    // declaro la función enviar
    function sendEmail(userEmail) {
      var defered = $q.defer(),
        promise = defered.promise,
        config = InitialConfigService.getConfig(),
        req = {
          method: 'GET',
          url: config.API.url +
              config.API.urlLostPasswordSendEmail + '?eMail=' + userEmail,
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          }
        };

      $http(req).then(function(response) {
        if (response.data.cresol.strCode != 'I1000') {
          defered.reject(response.data.cresol.code);
        } else {
          defered.resolve(response.data.cresol);
        }
      }, function(reason, status, headers, config) {
        defered.reject(reason.data.cresol.code);
      });
      return promise;
    }
    return {
      sendEmail: sendEmail
    };
  }

})();
