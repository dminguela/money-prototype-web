(function() {
  'use strict';

  /* @ngdoc object
   * @name checkLink.service:CheckLinkService
   *
   * @description
   *
   */

  angular
    .module('payLink')
    .service('CheckLinkService', CheckLinkService);

  function CheckLinkService($q, $http, $rootScope, localStorageService, AuthenticationService,
    InitialConfigService) {
    var fm = this;
    AuthenticationService.clearCredentials();
    function checkLink() {
      var deferred = $q.defer(),
        promise = deferred.promise,
        myResponse = {},
        config = InitialConfigService.getConfig(),
        linkData = localStorageService.get('deepLink') || false,
        req = {
          method: 'POST',
          url: config.API.url + config.API.urlCheckLink,
          headers: {
            'Content-Type': 'application/json; charset=UTF-8'
          },
          data: linkData
        };

      if (!linkData) {
        myResponse.code = 'NO LINK';
        deferred.reject(myResponse);
      } else {
        $http(req).then(function(response) {
          switch (response.data.infoResponse.code) {
            case 'OK':
              var userInformation = response.data.responseObject.userInformation;
              linkData.nameUser = userInformation.firstSurname;
              linkData.titleLink = userInformation.name;
              linkData.image = userInformation.imageUrl;
              linkData.code = response.data.infoResponse.code;
              localStorageService.set('deepLink', linkData);
              deferred.resolve(linkData);
              break;
            default:
              deferred.reject(response.data.infoResponse);
              break;
          }
        }, function(reason) {
          try {
            deferred.reject(reason.data.infoResponse);
          } catch (e) {
            reason.code = null;
            deferred.reject(reason);
          }
        });
      }

      return deferred.promise;
    }

    function setAmount(amount) {
      localStorageService.set('amount', amount);
      var link = localStorageService.get('deepLink');
      link.amount = amount;
      localStorageService.set('deepLink', link);
    }

    return {
      checkLink: checkLink,
      setAmount: setAmount
    };
  }
})();
