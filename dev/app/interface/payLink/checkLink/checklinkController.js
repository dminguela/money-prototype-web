(function() {
  'use strict';

  /* @ngdoc object
   * @name splash-screen.controller:CheckLinkController
   *
   * @description
   *
   */
  angular
    .module('payLink')
    .controller('CheckLinkController', CheckLinkController);

  function CheckLinkController(localStorageService, getDataBranch, CheckLinkService, $state,
    DataLayerService) {
    var vm = this;
    vm.errorNoData = false;

    vm.sendEventPush = function(_category, _action, _label) {
      DataLayerService.eventPush(_category, _action, _label);
    };

    switch (getDataBranch.code) {
      case 'OK':
        vm.deepLinkParameters = getDataBranch;
        $log.info(vm.deepLinkParameters);
      case 'NO LINK':
        vm.showOverlayNoLink = true;
        vm.sendEventPush('PayDeeplink', 'BlurredDeeplink[_Photo]', 'PayDeeplink-Blurred');
        break;
      case 'PAYMENT_LINK_OWNER_COULD_NOT_RECEIVE_MONEY':
        vm.showOverlay = true;
        vm.showOverlayText = getDataBranch.text;
        vm.sendEventPush('PayDeeplink', 'BlurredDeeplink[_Photo]', 'PayDeeplink-Blurred');
        break;
      default:
        if (getDataBranch.text) {
          vm.errorNoData = true;
          vm.errorNoDataText = getDataBranch.text;
        } else {
          vm.errorBadRequest = true;
        }
        break;
    }

    vm.setAmountAndGoLoginPay = function() {
      CheckLinkService.setAmount(vm.amount);
      $state.go('payLink.loginPay');
    };

  }

})();
