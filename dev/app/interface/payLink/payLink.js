(function() {
  'use strict';

  /* @ngdoc object
   * @name credentials
   * @requires $stateProvider
   *
   * @description
   *
   */
  angular
    .module('payLink', [
      'ui.router'
    ])
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('payLink', {
        url: '/payLink',
        abstract: true,
        templateUrl: 'interface/payLink/payLink.tpl.html',
        controller: 'PayLinkController as payLink'
      })
      .state('payLink.checkLink', {
        url: '/checkLink',
        templateUrl: 'interface/payLink/checkLink/checkLink.tpl.html',
        controller: 'CheckLinkController as checkLink',
        resolve: {
          getDataBranch: function($q, Branchio, CheckLinkService) {
            var deferred = $q.defer();
            Branchio.getDataBranch().then(function() {
              CheckLinkService.checkLink().then(function(dataLink) {
                deferred.resolve(dataLink);
              }).catch(function(reason) {
                deferred.resolve(reason);
              });
            }).catch(function(reason) {
              deferred.resolve(reason);
            });
            return deferred.promise;
          }
        }
      })
      .state('payLink.loginPay', {
        url: '/loginPay',
        templateUrl: 'interface/payLink/loginPay/loginPay.tpl.html',
        controller: 'LoginPayController as loginPay'
      })
      .state('payLink.confirmPay', {
        url: '/confirmPay',
        templateUrl: 'interface/payLink/confirmPay/confirmPay.tpl.html',
        controller: 'ConfirmPayController as confirmPay'
      });
  }

})();
