(function() {
  'use strict';

  /* @ngdoc object
   * @name controller:RegisterService
   *
   * @description
   *
   */

  angular
    .module('config', [])
    .factory('InitialConfigService', InitialConfigService);

  function InitialConfigService($http, $q, $rootScope) {
    // declaro la función enviar
    var fm = this,
      config;

    function getInitialConfig() {
      var defered = $q.defer(),
      req = {
        method: 'GET',
        url: 'config/config.json',
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
      };

      $http(req).then(function(result) {
        config = result.data;
        defered.resolve(config);
      }, function(reason) {
        config = null;
        defered.resolve();
      });

      return defered.promise;
    }

    function getConfig() {
      if (config === undefined) {
        getInitialConfig().then(function() {
          return config;
        });
      } else {
        return config;
      }
    }

    return {
      getInitialConfig: getInitialConfig,
      getConfig: getConfig
    };
  }
})();
