'use strict';

var gulp = require('gulp')
  , path = require('path')
  , $ = require('gulp-load-plugins')({
    pattern: [
      'gulp-*',
      'browser-sync'
    ]
  })

  , buildConfig = require('../build.config.js')

  , appFiles = path.join(buildConfig.appDir, '**/*');

gulp.task('browserSync', function () {
  $.browserSync({
    host: buildConfig.host,
    open: 'external',
    ui: {
      port: 3004
    },
    port: buildConfig.port,
    server: {
      baseDir: buildConfig.projectServer
    }
  });
});

gulp.task('watch', function () {
  $.browserSync.reload();
  gulp.watch([appFiles], ['build', $.browserSync.reload]);
});

