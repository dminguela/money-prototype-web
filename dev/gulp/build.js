'use strict';

var gulp = require('gulp'),
	path = require('path'),
	mainBowerFiles = require('main-bower-files'),
	imageminSvgo = require('imagemin-svgo'),
	imageminJpegtran = require('imagemin-jpegtran'),
	imageminOptipng = require('imagemin-optipng'),
	minifyCSS = require('gulp-minify-css'),
	$ = require('gulp-load-plugins')({
		pattern: [
			'gulp-*',
			'bower-files',
			'uglify',
			'rimraf'
		]
	}),

	buildConfig = require('../build.config.js'),

	appBase = buildConfig.appDir,
	appConfigDir = path.join(appBase, 'config/'),
	appFontFiles = path.join(appBase, 'fonts/**/*'),
	appImagesJpg = path.join(appBase, 'images/**/*.jpg'),
	appImagesPng = path.join(appBase, 'images/**/*.png'),
	appImagesSvg = path.join(appBase, 'images/**/*.svg'),
	appMarkupFiles = path.join(appBase, '**/*.html'),
	appScriptFiles = path.join(appBase, '**/*.js'),
	appStyleCssFiles = path.join(appBase, '**/*.css'),
	appStyleSassFiles = path.join(appBase, '**/*.sass'),
	appLanguagesFiles = path.join(appBase, 'i18n/*.json'),
	appLibsFiles = path.join(appBase, 'libs/*.js'),
	configJson,
	ofuscate = false;

switch ($.util.env.env) {
	case 'pro':
		configJson = appConfigDir + "configPro.json";
		ofuscate = true;
		break;
	case 'pre':
		configJson = appConfigDir + "configPre.json";
		ofuscate = true;
		break;
	default:
		configJson = appConfigDir + "configInt.json";
		ofuscate = false;
		break;
}

// delete build directory
gulp.task('clean', function(cb) {
	return $.rimraf(buildConfig.buildDir, cb);
});

gulp.task('config', function() {
	return gulp.src(configJson)
		.pipe($.rename('config.json'))
		.pipe(gulp.dest(buildConfig.buildAppConfig));
});

gulp.task('scripts', ['libs'], function() {
	return gulp.src([appScriptFiles, '!' + appLibsFiles])
		.pipe($.jscs({
			configPath: "./.jscsrc"
		}))
		.pipe($.jscs.reporter())
		.pipe($.jscs.reporter('fail'))
		.pipe($.ngAnnotate())
		.pipe(ofuscate ? $.uglify() : $.util.noop())
		.pipe($.concat('money.min.js'))
		.pipe(gulp.dest(buildConfig.buildJs));
});

gulp.task('libs', function(){
	return gulp.src(appLibsFiles)
		.pipe(ofuscate ? $.uglify() : $.util.noop())
		.pipe($.concat('libs.min.js'))
		.pipe(gulp.dest(buildConfig.buildJs));
});

gulp.task('sass', function() {
	return gulp.src(appStyleSassFiles)
		.pipe($.sass().on('error', $.sass.logError))
		.pipe($.flatten())
		.pipe(minifyCSS())
		.pipe($.autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
		.pipe($.concat('money.min.css'))
		.pipe(gulp.dest(buildConfig.buildCss));
});

gulp.task('style', ['sass'], function() {
	return gulp.src(appStyleCssFiles)
		.pipe($.flatten())
		.pipe(gulp.dest(buildConfig.buildCssVendor));
});

gulp.task('imagesPng', function() {
	return gulp.src(appImagesPng)
		.pipe(imageminOptipng({
			optimizationLevel: 3
		})())
		.pipe(gulp.dest(buildConfig.buildImages));
});

gulp.task('imagesJpg', function() {
	return gulp.src(appImagesJpg)
		.pipe(imageminJpegtran({
			progressive: true
		})())
		.pipe(gulp.dest(buildConfig.buildImages));
});

gulp.task('imagesSvg', function() {
	return gulp.src(appImagesSvg)
		.pipe(imageminSvgo()())
		.pipe(gulp.dest(buildConfig.buildImages));
});

// Copy all other files to dist directly
gulp.task('languages', function() {
	return gulp.src(appLanguagesFiles)
		.pipe(gulp.dest(buildConfig.buildLanguages));
});

gulp.task('fonts', function() {
	return gulp.src(appFontFiles)
		.pipe(gulp.dest(buildConfig.buildFonts));
});

// Copy all other files to dist directly
gulp.task('markup', ['languages', 'fonts'], function() {
	return gulp.src(appMarkupFiles)
		.pipe($.htmlmin({collapseWhitespace: true}))
		.pipe(gulp.dest(buildConfig.buildDir));
});

gulp.task('bowerCopyJs', ['markup', 'scripts', 'imagesPng', 'imagesJpg', 'imagesSvg', 'style'], function() {
	return gulp.src(mainBowerFiles('**/*.js'))
		.pipe(gulp.dest(buildConfig.buildJsVendorAux));
});

gulp.task('bowerInjectJs', ['bowerCopyJs', 'bowerCopyCss'], function() {
	var baseStream = gulp.src([
			buildConfig.buildJsVendorAux + 'angular.js',
			buildConfig.buildJsVendorAux + 'jquery.js'
		])
		.pipe($.angularFilesort())
		.pipe($.concat('base.money.min.js'))
		.pipe(gulp.dest(buildConfig.buildJsVendor));

	var angularModulesStream = gulp.src([
			buildConfig.buildJsVendorAux + 'angular-*.js'
		])
		.pipe($.angularFilesort())
		.pipe($.concat('angularModules.money.min.js'))
		.pipe(gulp.dest(buildConfig.buildJsVendor));

	

	var othersStream = gulp.src([
			'!' + buildConfig.buildJsVendorAux + 'angular-*.js',
			'!' + buildConfig.buildJsVendorAux + 'angular.js',
			'!' + buildConfig.buildJsVendorAux + 'jquery.js',
			buildConfig.buildJsVendorAux + '*.js'
		])
		.pipe($.angularFilesort())
		.pipe($.concat('others.money.min.js'))
		.pipe(gulp.dest(buildConfig.buildJsVendor));

	return gulp.src(buildConfig.buildDir + 'index.html')
		.pipe($.inject(baseStream, {
			starttag: '<!-- inject:base:{{ext}} -->',
			relative: true
		}))
		.pipe($.inject(angularModulesStream, {
			starttag: '<!-- inject:angularModules:{{ext}} -->',
			relative: true
		}))
		.pipe($.inject(othersStream, {
			starttag: '<!-- inject:others:{{ext}} -->',
			relative: true
		}))
		.pipe($.inject(gulp.src([ buildConfig.buildDir + 'js/libs.min.js', buildConfig.buildDir + 'js/money.min.js']), {
			relative: true
		}))
		.pipe(gulp.dest(buildConfig.buildDir));
});

gulp.task('bowerCleanJs', ['bowerInjectJs'], function(cb) {
	return $.rimraf(buildConfig.buildJsVendorAux, cb);
});

gulp.task('bowerCopyCss', function() {
	return gulp.src(mainBowerFiles('**/*.css'))
		.pipe(gulp.dest(buildConfig.buildCssVendor));
});

gulp.task('bowerInjectCss', ['bowerCleanJs'], function() {
	return gulp.src(buildConfig.buildDir + 'index.html')
		.pipe($.inject(gulp.src(
			[
				buildConfig.buildDir + 'css/vendors/font-awesome.min.css',
				buildConfig.buildDir + 'css/vendors/*.css',
			]), {
			starttag: '<!-- inject:head:{{ext}} -->',
			relative: true
		}))
		.pipe(gulp.dest(buildConfig.buildDir));
});

gulp.task('injectCss', ['bowerInjectCss'], function() {
	return gulp.src(buildConfig.buildDir + 'index.html')
		.pipe($.inject(gulp.src(buildConfig.buildDir + 'css/money.min.css'), {
			relative: true
		}))
		.pipe(gulp.dest(buildConfig.buildDir));
});

gulp.task('cleanBeforeBuild', ['clean']);
gulp.task('build', ['injectCss', 'config']);